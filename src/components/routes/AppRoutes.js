import React from 'react';
import Navbar from "../Navbar/Navbar";
import ListItems from "../ListItems/ListItems";
import {Redirect, Route} from 'react-router-dom';
import Cart from "../../pages/Cart/Cart";
import Favorites from "../../pages/Favorites/Favorites";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Empty from "../Empty/Empty";

const AppRoutes = ({onClick, deleteCard, addToFavorites, deleteFavorites, cardsCart, cardsFavorites}) => {

       return (
        <>
            <Navbar />
            <Route exact path='/cart' render={(props)=> cardsCart.length !== 0 ? <Cart
                                                          deleteCard = {deleteCard}
                                                          page={props.match.path}
                                                       /> : <Empty/>}/>
            <Route exact path='/favorites' render={(props)=> cardsFavorites.length !== 0 ? <Favorites
                                                               deleteFavorites = {deleteFavorites}
                                                               page={props.match.path}
                                                             /> : <Empty/>}/>
            <Route exact path='/main' render={(props)=><ListItems
                                                           onClick={onClick}
                                                           addToFavorites={addToFavorites}
                                                           deleteFavorites={deleteFavorites}
                                                           page={props.match.path}
                                                        />} />
            <Route exact path='/' render={(props)=><Redirect to='/main' />} />
        </>
    );
};
AppRoutes.propTypes = {
    onClick: PropTypes.func.isRequired,
    deleteCard: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    deleteFavorites: PropTypes.func.isRequired,
    cardsCart: PropTypes.array.isRequired,
    cardsFavorites: PropTypes.array.isRequired
};
const mapStoreToProps = ({cardsCart, cardsFavorites}) => {
    return{
        cardsCart,
        cardsFavorites
    }
};

export default connect(mapStoreToProps)(AppRoutes);
