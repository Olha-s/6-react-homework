import React from 'react';
import CardItem from "../../components/CardItem/CardItem";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import MyForm from "../../components/MyForm/MyForm";

const Cart = ({cardsCart, page, deleteCard}) => {

    const cards = cardsCart.map(el => <CardItem card={el} key={el.article} page={page} deleteCard={deleteCard}/>);
    return (
        <div>
            <MyForm/>
            <div className="ListItems">
                {cards}
            </div>
        </div>
    );
};

Cart.propTypes = {
    deleteCard: PropTypes.func.isRequired,
    cardsCart: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired
};

const mapStoreToProps = ({cardsCart}) => {
    return {
        cardsCart
    }
};
export default connect(mapStoreToProps)(Cart);