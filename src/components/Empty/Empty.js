import React from 'react';
import './Empty.scss';

const Empty = () => {
    return (
        <div>
            <p className = "text__empty">
                No items yet!
            </p>
        </div>
    );
};

export default Empty;