import React from "react";
import { fireEvent, render } from "@testing-library/react";
import { CardItem } from "./CardItem";

jest.mock("../FavoritesIcon/FavoritesIcon", () => (props) => (
  <>FavoritesIcon</>
));

describe("Testing buttons 'Add to cart'", () => {
  test("Card contains button 'Add to cart'", () => {
    const mockPage = "/main";
    const { queryAllByText } = render(<CardItem page={mockPage} />);
    const btnAdd = queryAllByText("Add to cart");
  });
  test("Simple CardItem snapshot to check button 'Add to cart' is rendered", () => {
    const mockPage = "/main";
    const { container } = render(<CardItem page={mockPage} />);
    expect(container.innerHTML).toMatchInlineSnapshot(
      `"<div class=\\"card__wrapper\\"><div class=\\"card\\">FavoritesIcon<div class=\\"card__img-wrapper\\"><img class=\\"card__img\\" alt=\\"\\"></div><div class=\\"card__info-wrapper\\"><p class=\\"card__name\\"></p><p class=\\"card__article\\">Артикль </p><p class=\\"card__color\\">цвет: по запросу</p><div class=\\"card__footer\\"><p class=\\"card__price\\"></p><button data-testid=\\"btn-add\\">Add to cart</button></div></div></div></div>"`
    );
  });
  test("Function onClick is called when was clicked on button 'Add to cart'", () => {
    const mockPage = "/main";
    const btnClickMock = jest.fn();
    const { getByTestId } = render(
      <CardItem page={mockPage} onClick={btnClickMock} />
    );

    const btnAdd = getByTestId("btn-add");
    expect(btnClickMock).not.toHaveBeenCalled();
    fireEvent.click(btnAdd);
    expect(btnClickMock).toHaveBeenCalledTimes(1);
  });
});
describe("Testing buttons cross", () => {
  test("Card contains button cross", () => {
    const mockPage = "/cart";
    const { getByTestId } = render(<CardItem page={mockPage} />);
    const btnCross = getByTestId("btn-cross-delete");
  });
  test("Simple CardItem snapshot to check button cross is rendered", () => {
    const mockPage = "/cart";
    const { container } = render(<CardItem page={mockPage} />);
    expect(container.innerHTML).toMatchInlineSnapshot(
      `"<div class=\\"card__wrapper\\"><div class=\\"card\\"><div class=\\"card__img-wrapper\\"><img class=\\"card__img\\" alt=\\"\\"></div><div class=\\"card__info-wrapper\\"><p class=\\"card__name\\"></p><p class=\\"card__article\\">Артикль </p><p class=\\"card__color\\">цвет: по запросу</p><div class=\\"card__footer\\"><p class=\\"card__price\\"></p></div></div></div><span data-testid=\\"btn-cross-delete\\" class=\\"card__delete\\"><img src=\\"cross.png\\" alt=\\"cross\\" class=\\"card__img-cross\\"></span></div>"`
    );
  });
  test("Function onClick is called when was clicked on button cross", () => {
    const mockPage = "/cart";
    const btnCrossClickMock = jest.fn();
    const { getByTestId } = render(
      <CardItem page={mockPage} deleteCard={btnCrossClickMock} />
    );
    const btnCross = getByTestId("btn-cross-delete");
    expect(btnCrossClickMock).not.toHaveBeenCalled();
    fireEvent.click(btnCross);
    expect(btnCrossClickMock).toHaveBeenCalledTimes(1);
  });
});
