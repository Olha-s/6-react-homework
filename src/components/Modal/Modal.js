import React from 'react';
import './Modal.scss';
import cross from '../../images/cross.png';
import PropTypes from "prop-types";

const Modal = ({onClick, header, text, actions}) => {

    const wrapperClose = (e) => {
        const atrClass = e.target.getAttribute('class');
        if (atrClass === "ModalWrapper" || atrClass === "Modal__btn" || atrClass === "Modal__cross") {
            onClick();
           }
    };
    return (
        <div data-testid='modal-wrapper' className="ModalWrapper" onClick={wrapperClose}>
            <div className="Modal">
                <div data-testid='modal-header' className="Modal__header">
                    <p className="Modal__header-text">{header}</p>
                    <span><img data-testid='modal-img' src={cross} alt="cross" className="Modal__cross"/></span>
                </div>
                <div>
                    <p className="Modal__main">{text}</p>
                    <div data-testid='modal-buttons' className="Modal__buttons">{actions}</div>
                </div>
            </div>
        </div>
    );
};


Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Modal;