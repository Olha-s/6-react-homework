import React from "react";
import {render, fireEvent} from "@testing-library/react";
import Modal from "./Modal";

describe("Testing Modal.js", () => {
    test("Modal is rendered is correctly", () => {
        render(<Modal/>)
    });

    test("Modal contains header", () => {
        const {getByTestId} = render(<Modal/>);
        const modalHeader = getByTestId("modal-header");
        expect(modalHeader).toBeDefined();
        expect(modalHeader).toHaveClass("Modal__header");
    });

    test("Simple Modal snapshot", () => {
        const {container} = render(<Modal/>);
        expect(container.innerHTML).toMatchInlineSnapshot(
            `"<div data-testid=\\"modal-wrapper\\" class=\\"ModalWrapper\\"><div class=\\"Modal\\"><div data-testid=\\"modal-header\\" class=\\"Modal__header\\"><p class=\\"Modal__header-text\\"></p><span><img data-testid=\\"modal-img\\" src=\\"cross.png\\" alt=\\"cross\\" class=\\"Modal__cross\\"></span></div><div><p class=\\"Modal__main\\"></p><div data-testid=\\"modal-buttons\\" class=\\"Modal__buttons\\"></div></div></div></div>"`
        );
    });

    test("Function onClick is called when was clicked on button or wrapper or cross", () => {
        const modalClickMock = jest.fn();
        const {getByTestId} = render(<Modal onClick={modalClickMock}/>);
        const modalWrapper = getByTestId("modal-wrapper");
        const modalCrossImg = getByTestId("modal-img");
        const modalBtn = getByTestId("modal-buttons").children;

        expect(modalClickMock).not.toHaveBeenCalled();

        fireEvent.click(modalWrapper || modalCrossImg || modalBtn);
        expect(modalClickMock).toHaveBeenCalledTimes(1);
    });

});
