import React from "react";
import { render, fireEvent } from "@testing-library/react";
import FavoritesIcon from "./FavoritesIcon";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import initialStore from "../../store/initialStore";

const mockStore = configureStore();
const store = mockStore(initialStore);

describe("Testing FavoritesIcon", () => {
  test("FavoritesIcon is rendered is correctly", () => {
    const mockPage = "/main";
    render(
      <Provider store={store}>
        <FavoritesIcon cardsFavorites page={mockPage} />
      </Provider>
    );
  });

  test("Simple FavoritesIcon snapshot", () => {
    const mockPage = "/main";
    const { container } = render(
      <Provider store={store}>
        <FavoritesIcon cardsFavorites page={mockPage} />
      </Provider>
    );
    expect(container.innerHTML).toMatchInlineSnapshot(
      `"<div data-testid=\\"btn-star\\"><svg id=\\"star\\" xmlns=\\"http://www.w3.org/2000/svg\\" width=\\"30\\" height=\\"30\\" viewBox=\\"0 0 48 48\\" fill=\\"none\\" cursor=\\"pointer\\"><path d=\\"M34.865,39.83l-10.25-5.621-10.153,5.8,2.091-11.647L7.99,20.335l11.542-1.577L24.394,8l5.042,10.672L41,20.047l-8.426,8.173Z\\" stroke-width=\\"3\\" stroke=\\"#8d81ac\\" fill=\\"none\\"></path></svg></div><div data-testid=\\"btn-star-del\\"></div>"`
    );
  });

  test("Function addToFavorites is called when was clicked on not filled button-star", () => {
    const mockPage = "/main";
    const btnStarClickMock = jest.fn();
    const { getByTestId } = render(
      <Provider store={store}>
        <FavoritesIcon
          addToFavorites={btnStarClickMock}
          cardsFavorites
          page={mockPage}
        />
      </Provider>
    );

    const btnStar = getByTestId("btn-star");
    expect(btnStarClickMock).not.toHaveBeenCalled();
    fireEvent.click(btnStar);
    expect(btnStarClickMock).toHaveBeenCalledTimes(1);
  });

  test("Function deleteFavorites is called when was clicked on filled button-star", () => {
    const mockPage = "/favorites";
    const btnStarFilledClickMock = jest.fn();
    const { getByTestId } = render(
      <Provider store={store}>
        <FavoritesIcon
          deleteFavorites={btnStarFilledClickMock}
          cardsFavorites
          page={mockPage}
        />
      </Provider>
    );

    const btnStar = getByTestId("btn-star-del");
    expect(btnStarFilledClickMock).not.toHaveBeenCalled();
    fireEvent.click(btnStar);
    expect(btnStarFilledClickMock).toHaveBeenCalledTimes(1);
  });
});
