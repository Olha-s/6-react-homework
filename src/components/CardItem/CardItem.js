import React from 'react';
import PropTypes from 'prop-types';
import './CardItem.scss';
import FavoritesIcon from "../FavoritesIcon/FavoritesIcon";
import cross from '../../images/cross.png';

export const CardItem = ({card: {name, price, src, article, color}, onClick, page, deleteCard, addToFavorites, deleteFavorites}) => {

    const onBtnClick = () => {
        onClick(article);
    };
    const onCrossClick = () => {
        deleteCard(article)
    };

    return (
        <div className="card__wrapper">
            <div className="card">
                {
                    (page === '/main' || page === '/favorites')
                    && <FavoritesIcon
                        article={article}
                        name={name}
                        addToFavorites={addToFavorites}
                        deleteFavorites={deleteFavorites}
                        page={page}
                    />
                }
                <div className="card__img-wrapper">
                    <img className="card__img" src={src} alt=""/>
                </div>
                <div className="card__info-wrapper">
                    <p className="card__name">{name}</p>
                    <p className="card__article">Артикль {article}</p>
                    <p className="card__color">цвет: {color}</p>
                    <div className="card__footer">
                        <p className="card__price">{price}</p>
                        {page === "/main" && <button data-testid='btn-add' onClick={onBtnClick}>Add to cart</button>}
                    </div>
                </div>
            </div>
            {page === "/cart" && <span data-testid='btn-cross-delete' className="card__delete" onClick={onCrossClick}>
                <img src={cross} alt="cross" className="card__img-cross"/></span>}
        </div>
    );
};


CardItem.defaultProps = {
    card: {
        color: 'по запросу'
    }

};
CardItem.propTypes = {
    card: PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        src: PropTypes.string.isRequired,
        article: PropTypes.string.isRequired,
        color: PropTypes.string
    }),
    onClick: PropTypes.func,
    deleteCard: PropTypes.func,
    addToFavorites: PropTypes.func,
    deleteFavorites: PropTypes.func,
    page: PropTypes.string.isRequired
};

export default CardItem;

