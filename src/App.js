import React, {useEffect} from 'react';
import './App.scss';
import Modal from './components/Modal/Modal';
import AppRoutes from "./components/routes/AppRoutes";
import {connect} from "react-redux";
import {
    loadCardsAction,
    modalShowAction,
    modalShowCartAction,
    addCardsCartAction,
    deleteCardsCartAction,
    addCardsFavoritesAction,
    deleteCardsFavoritesAction
} from "./store/actions/actions";

const App = ({
                 items, loadCards, modalShow, modalOpen, modalShowCart, ModalCartOpen, cardsCart,
                 addCardsCart, deleteCardsCart, cardsFavorites, addCardsFavorites, deleteCardsFavorites
             }) => {

    useEffect(() => {
        loadCards()
    }, [loadCards]);

    const modalClose = () => {
        modalOpen(false);
    };
    const modalCloseCart = () => {
        ModalCartOpen(false);
    };

    const addToCart = (id) => {
        if (!cardsCart.find(card => card.article === id)) {
            modalOpen(true);
            const cardNew = items.filter(card => card.article === id);
            const [{...cardAdd}] = cardNew;
            addCardsCart(cardAdd);
        } else {
            alert('The item has already been added to the cart!')
        }
    };
    useEffect(() => {
        localStorage.setItem('Cart', JSON.stringify(cardsCart));
    }, [cardsCart]);


    const deleteCard = (id) => {
        ModalCartOpen(true);
        deleteCardsCart(id);
    };

    const addToFavorites = (id) => {
        if (!cardsFavorites.find(card => card.article === id)) {
            const cardNew = items.filter(card => card.article === id);
            const [{...favoriteAdd}] = cardNew;
            addCardsFavorites(favoriteAdd);
        }
    };

    const deleteFavorites = (id) => {
        deleteCardsFavorites(id);
    };

    return (
        <div className="App">
            <AppRoutes
                onClick={addToCart}
                deleteCard={deleteCard}
                addToFavorites={addToFavorites}
                deleteFavorites={deleteFavorites}
            />
            {modalShow && <Modal
                modalOne={true}
                onClick={modalClose}
                header={"Adding to cart"}
                closeButton={true}
                text={"The item has been successfully added to the cart"}
                actions={
                    <>
                        <button className={"Modal__btn"}>Ok</button>
                        <button className={"Modal__btn"}>Cancel</button>
                    </>
                }
            />}
            {modalShowCart && <Modal
                modalOne={true}
                onClick={modalCloseCart}
                header={"Do you want to delete this item?"}
                closeButton={true}
                text={"The item has been successfully remove from your cart!!!"}
                actions={
                    <>
                        <button className={"Modal__btn"}>Ok</button>
                        <button className={"Modal__btn"}>Cancel</button>
                    </>
                }
            />}
        </div>
    );
};
const mapStoreToProps = ({cardsCart, items, modalShow, modalShowCart, cardsFavorites}) => {
    return {
        items,
        modalShow,
        cardsCart,
        modalShowCart,
        cardsFavorites
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        loadCards: () => dispatch(loadCardsAction()),
        modalOpen: (isOpen) => dispatch(modalShowAction(isOpen)),
        ModalCartOpen: (isOpen) => dispatch(modalShowCartAction(isOpen)),
        addCardsCart: (cardAdd) => dispatch(addCardsCartAction(cardAdd)),
        deleteCardsCart: (id) => dispatch(deleteCardsCartAction(id)),
        addCardsFavorites: (cardAdd) => dispatch(addCardsFavoritesAction(cardAdd)),
        deleteCardsFavorites: (id) => dispatch(deleteCardsFavoritesAction(id))
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(App);
